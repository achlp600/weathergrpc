package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"weathergrpctest/weatherClient"
)

func TestConvert(t *testing.T) {
	weatherSupply := weatherClient.WeatherDTO{
		Main: weatherClient.Main{
			Temp:      288.15,
			FeelsLike: 286.15,
			Pressure:  1013.25,
			Humidity:  45,
		},
		Wind: weatherClient.Wind{Speed: 5},
	}

	expected := WeatherToClient{
		Temp:      15,
		FeelsLike: 13,
		Pressure:  760,
		Humidity:  45,
		WindSpeed: 5,
	}

	actual := convertWeather(weatherSupply)

	assert.Equal(t, expected, actual)
}
