# weatherGrpc

Simple weather server for grpc and CI/CD testing. Server uses OpenWeatherMap API

## Header level 2

Internal link [See code examples markup](#code-examples)!

Item list

- [ ] External link 1 [Create](https://gitlab.com/-/experiment/new_project_readme_content:83bcb9d50241761ba2abf2d481cf12c5?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file)
- [ ] External link 2 [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:83bcb9d50241761ba2abf2d481cf12c5?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line)


## Code examples

Code example
```
cd existing_repo
git remote add origin https://gitlab.com/achlp600/weathergrpc.git
git branch -M main
git push -uf origin main
```

Code example with language definition
```go
package main

import "fmt"

func main() {
	fmt.Println("Hello world!")
}
```
## Commands
To copy executable file
```shell
scp -i your_key_file source_file username@server_ip:/usr/local/bin
```

To copy *.service file
```shell
scp -i your_key_file source_file username@server_ip:/usr/lib/systemd/system
```