package main

import (
	"context"
	"github.com/labstack/echo/v4"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"math"
	"net"
	"net/http"
	"time"
	"weathergrpctest/msg"
	"weathergrpctest/weatherClient"
)

var WeatherStorage = make(map[string]weatherClient.WeatherDTO)
var WeatherClient = weatherClient.NewRestyWeatherClient()

func main() {
	// REST Server
	go func() {
		e := echo.New()

		// Example: /getcurrentweather?city=Dnipro
		e.GET("/getcurrentweather", CurrentWeather)

		e.Logger.Fatal(e.Start(":5745"))
	}()

	// GRPC Server
	listener, err := net.Listen("tcp", ":5750")
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}
	grpcServer := grpc.NewServer(opts...)

	msg.RegisterWeatherServer(grpcServer, &server{})
	grpcServer.Serve(listener)
}

func CurrentWeather(c echo.Context) error {
	city := c.QueryParam("city")
	tn := time.Now().Unix()
	weather, ok := WeatherStorage[city]
	if ok && weather.Dt > tn-3600*3 {
		return c.JSON(http.StatusOK, convertWeather(weather))
	}
	weatherSupply, err := WeatherClient.GetCurrentWeather(city)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	WeatherStorage[city] = weatherSupply
	return c.JSON(http.StatusOK, convertWeather(weatherSupply))
}

type server struct {
	msg.UnimplementedWeatherServer
}

func (server) GetCurrentWeather(ctx context.Context, city *msg.City) (*msg.CurrentWeather, error) {
	tn := time.Now().Unix()
	weather, ok := WeatherStorage[city.City]
	if ok && weather.Dt > tn-3600*3 {
		convertedWeather := convertWeather(weather)
		return &msg.CurrentWeather{
			Temp:      convertedWeather.Temp,
			Pressure:  convertedWeather.Pressure,
			Humidity:  convertedWeather.Humidity,
			Windspeed: convertedWeather.WindSpeed,
		}, nil
	}
	weatherSupply, err := WeatherClient.GetCurrentWeather(city.City)
	if err != nil {
		return nil, err
	}
	WeatherStorage[city.City] = weatherSupply
	convertedWeather := convertWeather(weather)
	return &msg.CurrentWeather{
		Temp:      convertedWeather.Temp,
		Pressure:  convertedWeather.Pressure,
		Humidity:  convertedWeather.Humidity,
		Windspeed: convertedWeather.WindSpeed,
	}, nil
}

type WeatherToClient struct {
	Temp      float64 `json:"temp"`
	FeelsLike float64 `json:"feels_like"`
	Pressure  float64 `json:"pressure"`
	Humidity  float64 `json:"humidity"`
	WindSpeed float64 `json:"windspeed"`
}

func convertWeather(input weatherClient.WeatherDTO) WeatherToClient {
	return WeatherToClient{
		Temp:      math.Round(input.Main.Temp - 273.15),
		FeelsLike: math.Round(input.Main.FeelsLike - 273.15),
		Pressure:  math.Round(input.Main.Pressure * 100 / 133.322368),
		Humidity:  input.Main.Humidity,
		WindSpeed: input.Wind.Speed,
	}
}
